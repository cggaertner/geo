/*	Copyright 2012 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#ifndef GEO_H_
#define GEO_H_

#include <math.h>

// internal macros
#define GEO_WGS84_A_ 6378137.0
#define GEO_WGS84_INV_F_ 298.2572229328697
#define GEO_BESSEL_A_ 6377397.155
#define GEO_BESSEL_INV_F_ 299.15281535132334
#define geo_B_(A, INV_F) ((A) * ((INV_F) - 1) / (INV_F))

// parameter constants for reference spheroids

static const double
	GEO_WGS84_A = GEO_WGS84_A_,
	GEO_WGS84_B = geo_B_(GEO_WGS84_A_, GEO_WGS84_INV_F_);

static const double
	GEO_BESSEL_A = GEO_BESSEL_A_,
	GEO_BESSEL_B = geo_B_(GEO_BESSEL_A_, GEO_BESSEL_INV_F_);

// helper functions

static inline double geo_radians(double deg, double min, double sec)
/*	convert from degrees, minutes, seconds to radians
*/
{
	static const double PI_180 = 3.14159265358979323846 / 180;
	return (deg + min / 60 + sec / 60 / 60) * PI_180;
}

static inline double geo_mean_radius(double a, double b)
/*	compute average radius of curvature
*/
{
	/*	use semicubic approximation of meridional mean for now
		TODO: look into approximations for the actual integral ∫∫ R dα dφ

		R = M·N/(N·cos²α + M·sin²α)
		M = N·b²/k²
		N = a²/k
		k = √(a²·cos²φ + b²·sin²φ)
	*/
	return pow((pow(a, 1.5) + pow(b, 1.5)) / 2.0, 2.0 / 3.0);
}

// fully generic distance functions

extern double geo_euclidean_distance_generic(
	double a, double b, double phi_1, double h_1, double phi_2, double h_2,
	double dLambda);
/*	euclidean distance between two arbitrary points
*/

extern double geo_vincenty_distance_generic(
	double a, double b, double phi_1, double phi_2, double dLambda,
	double eps, unsigned limit);
/*	geodesic distance between two surface points
	implementation courtesy of Chris Veness
*/

extern double geo_approximate_distance_generic(
	double a, double b, double phi_1, double phi_2, double dLambda,
	_Bool small);
/*	approximate geodesic distance between two surface points
*/

extern double geo_spherical_distance_generic(
	double R, double phi_1, double phi_2, double dLambda);
/*	geodesic distance between two surface points on a sphere
*/

extern double geo_differential_distance_generic(
	double a, double b, double phi, double dPhi, double dLambda);
/*	approximate geodesic distance between two surface points
	uses Pythagoras' theorem with local metric
	pretty much useless
*/

// convenience functions for distances on the surface of the WGS84 spheroid

extern double geo_euclidean_distance(
	double phi_1, double phi_2, double dLambda);

extern double geo_vincenty_distance(
	double phi_1, double phi_2, double dLambda);

extern double geo_approximate_distance(
	double phi_1, double phi_2, double dLambda, _Bool small);

extern double geo_spherical_distance(
	double phi_1, double phi_2, double dLambda);

extern double geo_differential_distance(
	double phi_1, double phi_2, double dLambda);

#endif
