/*	Copyright 2012 Christoph Gärtner
	Copyright 2002-2012 Chris Veness
	Distributed under the Boost Software License, Version 1.0
*/

#include "geo.h"

#define VINCENTY_EPS 1e-12
#define VINCENTY_LIMIT 100

static void cylindrics(
	double a, double b, double *r, double *z, double phi, double h)
{
	/*	k = √(a²·cos²φ + b²·sin²φ)
		r = (a²/k + h)·cosφ
		z = (b²/k + h)·sinφ
	*/

	double cos_phi = cos(phi);
	double sin_phi = sin(phi);

	double a2, b2, k;
	a2 = a * a;
	b2 = b * b;
	k = sqrt(a2 * cos_phi * cos_phi + b2 * sin_phi * sin_phi);

	*r = (a2 / k + h) * cos_phi;
	*z = (b2 / k + h) * sin_phi;
}

double geo_euclidean_distance_generic(
	double a, double b, double phi_1, double h_1, double phi_2, double h_2,
	double dLambda)
{
	double r_1, r_2, z_1, z_2;
	cylindrics(a, b, &r_1, &z_1, phi_1, h_1);
	cylindrics(a, b, &r_2, &z_2, phi_2, h_2);

	double scalar_xy = r_1 * r_2 * cos(dLambda);
	double dr2, dz2;
	dr2 = r_1 * r_1 + r_2 * r_2 - 2 * scalar_xy;
	dz2 = (z_1 - z_2) * (z_1 - z_2);

	return sqrt(dr2 + dz2);
}

double geo_euclidean_distance(
	double phi_1, double phi_2, double dLambda)
{
	return geo_euclidean_distance_generic(
		GEO_WGS84_A, GEO_WGS84_B, phi_1, 0, phi_2, 0, dLambda);
}

double geo_approximate_distance_generic(
	double a, double b, double phi_1, double phi_2, double dLambda,
	_Bool small)
{
	double r_1, r_2, z_1, z_2;
	cylindrics(a, b, &r_1, &z_1, phi_1, 0);
	cylindrics(a, b, &r_2, &z_2, phi_2, 0);

	double R_1 = sqrt(r_1 * r_1 + z_1 * z_1);
	double R_2 = sqrt(r_2 * r_2 + z_2 * z_2);
	double cos_dLambda = cos(dLambda);
	double scalar_xy = r_1 * r_2 * cos_dLambda;
	double cos_alpha = (scalar_xy + z_1 * z_2) / (R_1 * R_2);

	double R_m;
	if(small)
	{
		double dr2, dz2;
		dr2 = r_1 * r_1 + r_2 * r_2 - 2 * scalar_xy;
		dz2 = (z_1 - z_2) * (z_1 - z_2);
		R_m = sqrt((dr2 + dz2) / (2 * (1 - cos_alpha)));
	}
	else R_m = geo_mean_radius(a, b);

	return R_m * acos(cos_alpha);
}

double geo_approximate_distance(
	double phi_1, double phi_2, double dLambda, _Bool small)
{
	return geo_approximate_distance_generic(
		GEO_WGS84_A, GEO_WGS84_B, phi_1, phi_2, dLambda, small);
}

double geo_spherical_distance_generic(
	double R, double phi_1, double phi_2, double dLambda)
{
	double cos_alpha =
		sin(phi_1) * sin(phi_2) + cos(phi_1) * cos(phi_2) * cos(dLambda);

	return R * acos(cos_alpha);
}

double geo_spherical_distance(
	double phi_1, double phi_2, double dLambda)
{
	return geo_spherical_distance_generic(
		geo_mean_radius(GEO_WGS84_A, GEO_WGS84_B), phi_1, phi_2, dLambda);
}

double geo_differential_distance_generic(
	double a, double b, double phi, double dPhi, double dLambda)
{
	/*	ds² = M²·dφ² + N²·cos²φ·dλ²
		M = N·b²/k²
		N = a²/k
		k = √(a²·cos²φ + b²·sin²φ)
	*/

	double cos2_phi = cos(phi) * cos(phi);
	double sin2_phi = sin(phi) * sin(phi);

	double a2, b2, k2;
	a2 = a * a,
	b2 = b * b;
	k2 = a2 * cos2_phi + b2 * sin2_phi;

	double M2, N2;
	N2 = a2 * a2 / k2;
	M2 = N2 * b2 * b2 / k2 / k2;

	double dPhi2 = dPhi * dPhi;
	double dLambda2 = dLambda * dLambda;

	return sqrt(M2 * dPhi2 + N2 * cos2_phi * dLambda2);
}

double geo_differential_distance(
	double phi_1, double phi_2, double dLambda)
{
	double phi = (phi_1 + phi_2) / 2;
	double dPhi = phi_1 - phi_2;
	return geo_differential_distance_generic(
		GEO_WGS84_A, GEO_WGS84_B, phi, dPhi, dLambda);
}

// cf. http://www.movable-type.co.uk/scripts/latlong-vincenty.html
double geo_vincenty_distance_generic(
	double a, double b, double phi_1, double phi_2, double dLambda,
	double eps, unsigned limit)
{
	double L = dLambda;
	double f = (a - b) / a;

	double U_1 = atan((1 - f) * tan(phi_1));
	double U_2 = atan((1 - f) * tan(phi_2));

	double sin_U_1 = sin(U_1);
	double cos_U_1 = cos(U_1);
	double sin_U_2 = sin(U_2);
	double cos_U_2 = cos(U_2);

	double lambda = L, lambda_p, sigma, sin_sigma, cos_sigma, sin_alpha,
		cos2_alpha, cos_2sigma_m;

	do {
		double sin_lambda = sin(lambda);
		double cos_lambda = cos(lambda);

		sin_sigma = sqrt(
			(cos_U_2 * sin_lambda) * (cos_U_2 * sin_lambda) +
			(cos_U_1 * sin_U_2 - sin_U_1 * cos_U_2 * cos_lambda) *
			(cos_U_1 * sin_U_2 - sin_U_1 * cos_U_2 * cos_lambda));

		if(sin_sigma == 0) // co-incident points
			return 0;

		cos_sigma = sin_U_1 * sin_U_2 + cos_U_1 * cos_U_2 * cos_lambda;
		sigma = atan2(sin_sigma, cos_sigma);
		sin_alpha = cos_U_1 * cos_U_2 * sin_lambda / sin_sigma;
		cos2_alpha = 1 - sin_alpha * sin_alpha;
		cos_2sigma_m = cos_sigma - 2 * sin_U_1 * sin_U_2 / cos2_alpha;

		if(isnan(cos_2sigma_m)) // equatorial line: cos²α = 0
			cos_2sigma_m = 0;

		double C = f / 16 * cos2_alpha * (4 + f * (4 - 3 * cos2_alpha));

		lambda_p = lambda;
		lambda = L + (1 - C) * f * sin_alpha *
			(sigma + C * sin_sigma * (cos_2sigma_m + C * cos_sigma *
			(-1 + 2 * cos_2sigma_m * cos_2sigma_m)));
	}
	while(fabs(lambda - lambda_p) > eps && --limit);

	if(!limit) // formula failed to converge
		return NAN;

	double u2 = cos2_alpha * (a * a - b * b) / (b * b);
	double A = 1 + u2 / 16384 * (4096 + u2 * (-768 + u2 * (320 - 175 * u2)));
	double B = u2 / 1024 * (256 + u2 * (-128 + u2 * (74 - 47 * u2)));
	double dSigma = B * sin_sigma * (cos_2sigma_m + B / 4 *
		(cos_sigma * (-1 + 2 * cos_2sigma_m * cos_2sigma_m) -
		B / 6 * cos_2sigma_m * (-3 + 4 * sin_sigma * sin_sigma) *
		(-3 + 4 * cos_2sigma_m * cos_2sigma_m)));

	return b * A * (sigma - dSigma);
}

double geo_vincenty_distance(
	double phi_1, double phi_2, double dLambda)
{
	return geo_vincenty_distance_generic(
		GEO_WGS84_A, GEO_WGS84_B, phi_1, phi_2, dLambda,
		VINCENTY_EPS, VINCENTY_LIMIT);
}
